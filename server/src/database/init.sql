begin transaction;
CREATE TABLE users (
  id integer primary key autoincrement,
  isAdmin integer default 0,
  name varchar(30) not null unique,
  email varchar(40) not null unique,
  password varchar(100) not null
);

CREATE TABLE images (
  id integer primary key autoincrement,
  imageUrl varchar(100) not null,
  userId integer not null,
  FOREIGN KEY(userId) REFERENCES user(id)
);
commit;
