const multer = require('multer');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, `${__dirname}/../../public/uploads/images`);
  },
  filename(req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

const userController = require('../controllers/user');
const imageController = require('../controllers/image');
const authController = require('../controllers/authController');
const verifyToken = require('../controllers/verifyToken');

module.exports = (app) => {
  app.get('/users', userController.get);
  app.get('/images', imageController.get);
  app.get('/users/:id', userController.getById);
  app.get('/images/:id', imageController.getById);
  app.get('/logout', authController.logout);
  app.get('/userid', verifyToken, userController.getUserId);

  app.post('/users', userController.post, authController.login);
  app.post('/images', upload.array('uploaded_image'), imageController.post);
  app.post('/login', authController.login);
  app.post('/send_email', userController.send_email);
};
