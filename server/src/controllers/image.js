const db = require('../database');

exports.getById = async (req, res) => {
  try {
    const { id } = req.params;

    const images = await db.all('SELECT imageUrl FROM images where images.id = $id ', {
      $id: id,
    });

    if (!images) return res.status(400).send('Bad request');

    return res.status(200).json(images);
  } catch (e) {
    return res.status(500).send('Something went wrong/GeImageById');
  }
};

exports.get = async (_, res) => {
  try {
    const images = await db.all('SELECT * FROM images');
    return res.status(200).json(images);
  } catch (e) {
    return res.status(500).send('Something went wrong/getAllImages');
  }
};

// POST Add image.
exports.post = async (req, res) => {
  try {
    const { userId = 1 } = req.body;
    const files = [...req.files];

    if (!userId || !files.length) return res.status(400).send('Bad request/AddImages');

    let query = 'insert into images(imageUrl,userId) values';
    files.forEach((file) => {
      if (
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/gif'
      ) {
        query += `('${file.originalname}',${userId}),`;
      } else {
        res.status(404).json({
          error: "This format is not allowed , please upload file with '.png','.gif','.jpg'",
        });
      }
    });
    query = `${query.slice(0, -1)};`;

    const id = await db.run(query);
    return res.status(200).json({ id, userId, files });
  } catch (e) {
    return res.status(500).send(`Something went wrong/uploadImage ${e}`);
  }
};
