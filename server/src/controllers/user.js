const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const conf = require('../.gmailConfig.js');
const saltRounds = require('../config.js').SALT_ROUNDS;
const db = require('../database');

// GET All users
exports.get = async (_, res) => {
  try {
    const users = await db.all('SELECT * FROM users');
    return res.status(200).json(users);
  } catch (e) {
    return res.status(500).send('Something went wrong/getAllUsers');
  }
};

// GET User by ID
exports.getById = async (req, res) => {
  try {
    const { id } = req.params;
    const users = await db.get(
      'SELECT users.name as username, users.email FROM users where users.userId = $id',
      { $id: id },
    );

    if (!users) return res.status(400).send('Bad request');

    return res.status(200).json(users);
  } catch (e) {
    return res.status(500).send('Something went wrong/GetUserById');
  }
};

// GET User ID from Token
exports.getUserId = async (req, res) => {
  if (req.userId) {
    return res.status(200).json({ id: req.userId });
  }
  return res.status(404).json({ error: 'No token provided or no user found.' });
};

// POST Add user
// https://www.npmjs.com/package/bcrypt

exports.post = async (req, res, next) => {
  try {
    const { name, email, password } = req.body;
    if (!name || !email || !password) return res.status(400).send('Bad request/AddUser');

    bcrypt.hash(password, saltRounds, async (err, hash) => {
      if (err) {
        throw err;
      }
      await db
        .run(`INSERT INTO users(name, email, password) VALUES ($name, $email, $password)`, {
          $name: name,
          $email: email,
          $password: hash,
        })
        .then(() => {
          next();
        })
        .catch(() => {
          return res.status(404).json({ error: 'User already exists!' });
        });
    });

    // return because of ESLint error.
    return req;
  } catch (e) {
    return res.status(500).send('Something went wrong/addUser');
  }
};

// SEND EMAIL FROM CLIENT TO OWNER
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: conf.USER,
    pass: conf.PASS,
  },
});

transporter.verify((error) => {
  if (error) throw error;
});

exports.send_mail = (req, res) => {
  const message = `Name: ${req.body.name}\nEmail: ${req.body.email}\n\nMessage: ${req.body.message}`;
  const mailOptions = {
    from: conf.USER,
    to: 'rajnald94@gmail.com',
    subject: req.body.subject,
    text: message,
  };
  transporter.sendMail(mailOptions, (error) => {
    if (error) return res.status(500).send('There was a problem sending email to owner.');
    return res.status(200).json({ message: 'Email sent successfully!' });
  });
};
