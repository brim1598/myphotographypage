const packageJson = require('../package');
const env = require('../env');

module.exports = {
  NAME: packageJson.name,
  VERSION: packageJson.version,
  DESCRIPTION: packageJson.description,
  SALT_ROUNDS: 10,
  PRIVATE_KEY: 'I_am_the_1r0nM@n!',
  ENV: process.env.NODE_ENV || 'development',
  ...env,
};
